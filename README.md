## Backend
- Language: Java
- Framework: Spring boot
- ORM: Hibernate
- API management library: Swagger

**Building and starting API server**

- From Intellij: just use Run/ Debug button to build and start project
- Using command line (make sure you've installed Maven): `mvn spring-boot:run`
- REST API page: `localhost:8080/`, it will automatically redirect you to Swagger API page: `localhost:8080/swagger-ui.html`

## Database
- Schema name: `laptop_shop`
- Username: `lst_admin`
- Password: `z64&QtSs3RvB`

_Note:_ No need to create tables, tables will be generated when backend server is built

## Docker
If you don't want to install JDK, Maven, Postgres... on your machine, you can use Docker instead
- Follow [this tutorial](https://docs.docker.com/docker-for-windows/install/) to install Docker
- To start api server and database altogether:
    - From your command line/ terminal, `cd` to backend folder (`<your wokrplace dir>\laptop-shop\laptop-shop`)
    - Build and start api server at the first time `docker-compose up --build`
    - After that, just use `docker-compose up`
    - Access Swagger API page at `localhost:8080/`
- Access Postgres database:
    - Install pgAdmin 4 or Command line tools: https://www.postgresqltutorial.com/install-postgresql/
    - At step 4, no need to select PostgreSQL Server (this service is already available in Docker setup)

## Frontend
- Framework: [`VueJs`](https://vuejs.org/v2/guide/)
- Material design framework: [`Vuetify`](https://vuetifyjs.com/en/), version: `2.1.1`
- Install Vue CLI: [link](https://cli.vuejs.org/guide/installation.html)

### Web-app
- Sales site for end user

**Setup and run application**

- Install dependencies: `npm install`
- Run application (dev mode):
    - Dev mode: `npm run start`
    - Production mode: `npm run build`
- Web app page: `localhost:9000/`

### Admin CMS
- Content management system for the administrator who manages the products

**Setup and run application**

- Install dependencies: `npm install`
- Run application: `npm run serve`
- CMS page: `localhost:8070/`