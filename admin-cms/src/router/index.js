import Vue from 'vue';
import Router from 'vue-router';

import store from '../store';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      name: 'login',
      path: '/login',
      component: lazyLoad('Login')
    },
    {
      name: 'main',
      path: '/',
      component: lazyLoad('Main'),
      meta: {
        authRequired: true
      },
      children: [
        {
          path: 'category',
          component: lazyLoad('Category'),
          meta: {
            authRequired: true
          }
        },
        {
          path: 'brand',
          component: lazyLoad('Brand'),
          meta: {
            authRequired: true
          }
        },
        {
          path: 'product',
          component: lazyLoad('Product'),
          meta: {
            authRequired: true
          }
        }
      ]
    }
  ]
});

function lazyLoad(view) {
  return () => import(`@/views/${view}`);
}

router.beforeEach((to, from, next) => {
  const isAuthRequiredRoute = to.matched.some(
    record => record.meta.authRequired
  );

  if (isAuthRequiredRoute && !store.getters['auth/isLoggedIn']) {
    next('/login');
  } else {
    next();
  }
});

export default router;
