import BaseService from './base.service';

const resource = '/brands';

class BrandService {
  getAll() {
    return BaseService.get(`${resource}`);
  }

  create(payload) {
    return BaseService.post(`${resource}`, payload);
  }

  update(id, payload) {
    return BaseService.put(`${resource}/${id}`, payload);
  }

  delete(id) {
    return BaseService.delete(`${resource}/${id}`);
  }
}

export default new BrandService();
