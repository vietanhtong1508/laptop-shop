import BaseService from './base.service';

const resource = '/files';

class FileService {
  upload(file) {
    let formData = new FormData();
    formData.append('file', file);

    return BaseService.post(`${resource}/upload`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
  }

  download(fileName) {
    return BaseService.get(`${resource}/download/${fileName}`);
  }

  delete(fileName) {
    return BaseService.delete(`${resource}/delete/${fileName}`);
  }
}

export default new FileService();
