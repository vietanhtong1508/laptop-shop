import BaseService from './base.service';

const resource = '/auth';

class AuthService {
  register() {}

  login({ username, password }) {
    return BaseService.post(`${resource}/login`, { username, password });
  }

  logout() {
    // TODO: remove accessToken
  }
}

export default new AuthService();
