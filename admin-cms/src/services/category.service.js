import BaseService from './base.service';

const resource = '/categories';

class CategoryService {
  getAll() {
    return BaseService.get(`${resource}`);
  }

  getParentCategories() {
    return BaseService.get(`${resource}/parent`);
  }

  getSubCategories(parentId) {
    return BaseService.get(`${resource}/sub`, {
      params: {
        parentId
      }
    });
  }

  create(payload) {
    return BaseService.post(`${resource}`, payload);
  }

  update(id, payload) {
    return BaseService.put(`${resource}/${id}`, payload);
  }

  delete(id) {
    return BaseService.delete(`${resource}/${id}`);
  }
}

export default new CategoryService();
