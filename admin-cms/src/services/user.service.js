import Repository from '../repository/repository';

const resource = '/user';

class UserService {
  getPublicContent() {
    return Repository.get(`${resource}/all`);
  }

  getUserContent() {
    return Repository.get(`${resource}/customer`);
  }

  getAdminBoard() {
    return Repository.get(`${resource}/admin`);
  }
}

export default new UserService();
