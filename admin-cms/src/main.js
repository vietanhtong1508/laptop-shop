import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import router from './router';
import store from './store';

import 'vuetify/dist/vuetify.min.css';

Vue.config.productionTip = false;

const EventBus = new Vue();
Vue.prototype.$eventBus = EventBus;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app');
