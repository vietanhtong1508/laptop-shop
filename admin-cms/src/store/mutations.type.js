export const REGISTER_SUCCESS = 'registerSuccess';
export const REGISTER_FAILURE = 'registerFailure';
export const SET_AUTH = 'setAuth';
export const LOGIN_FAILURE = 'loginFailure';
export const REMOVE_AUTH = 'removeAuth';

export const FETCH_START = 'setLoading';
export const FETCH_END = 'setData';
export const ADD_ITEM = 'addItem';
export const UPDATE_ITEM = 'updateItem';
export const DELETE_ITEM = 'deleteItem';
export const GET_UPDATED_INDEX = 'getUpdatedIndex';

export const SET_PARENT_CATEGORIES = 'setParentCategories';
export const SET_SUB_CATEGORIES = 'setSubCategories';
