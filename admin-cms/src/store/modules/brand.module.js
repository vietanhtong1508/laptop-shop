import BrandService from '@/services/brand.service';
import { FETCH_ALL, CREATE, UPDATE, DELETE } from '../actions.type';
import {
  FETCH_START,
  FETCH_END,
  ADD_ITEM,
  UPDATE_ITEM,
  GET_UPDATED_INDEX,
  DELETE_ITEM
} from '../mutations.type';

export const brand = {
  namespaced: true,

  state: {
    brands: [],
    updatedIndex: -1,
    resultMessage: ''
  },

  mutations: {
    [FETCH_END](state, brands) {
      state.brands = brands;
    },

    [ADD_ITEM](state, newBrand) {
      state.brands.push(newBrand);
      state.resultMessage = 'Added successfully';
    },

    [GET_UPDATED_INDEX](state, id) {
      state.updatedIndex = state.brands.findIndex(item => item.id === id);
    },

    [UPDATE_ITEM](state, updatedBrand) {
      Object.assign(state.brands[state.updatedIndex], updatedBrand);
      state.resultMessage = 'Updated successfully';
    },

    [DELETE_ITEM](state, brandId) {
      const deletedItem = state.brands.find(b => b.id === brandId);
      const deletedIndex = state.brands.indexOf(deletedItem);
      state.brands.splice(deletedIndex, 1);
      state.resultMessage = 'Deleted successfully';
    }
  },

  actions: {
    async [FETCH_ALL]({ commit }) {
      commit(FETCH_START, null, { root: true });
      try {
        const { data } = await BrandService.getAll();
        commit(FETCH_END, data);
        commit(FETCH_END, null, { root: true });
      } catch (error) {
        throw new Error(error);
      }
    },

    async [CREATE]({ commit }, newBrand) {
      try {
        const { data } = await BrandService.create(newBrand);
        commit(ADD_ITEM, data);
      } catch (error) {
        throw new Error(error);
      }
    },

    async [UPDATE]({ commit }, brandToUpdate) {
      try {
        const { id } = brandToUpdate;
        commit(GET_UPDATED_INDEX, id);
        const { data } = await BrandService.update(id, {
          name: brandToUpdate.name
        });
        commit(UPDATE_ITEM, data);
      } catch (error) {
        throw new Error(error);
      }
    },

    async [DELETE]({ commit }, brandId) {
      try {
        await BrandService.delete(brandId);
        commit(DELETE_ITEM, brandId);
      } catch (error) {
        throw new Error(error);
      }
    }
  },

  getters: {
    brands: state => {
      return state.brands.map(brand => {
        return {
          id: brand.id,
          logo: brand.logo
            ? `${process.env.VUE_APP_ROOT_API}/files/download/${brand.logo}`
            : '',
          name: brand.name
        };
      });
    }
  }
};
