import AuthService from '../../services/auth.service';
import { REGISTER, LOGIN, LOGOUT } from '../actions.type';
import {
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  SET_AUTH,
  LOGIN_FAILURE,
  REMOVE_AUTH
} from '../mutations.type';

export const auth = {
  namespaced: true,
  state: {
    user: null,
    isLoggedIn: false,
    error: null
  },
  mutations: {
    [REGISTER_SUCCESS](state) {
      state.isLoggedIn = false;
    },

    [REGISTER_FAILURE](state) {
      state.isLoggedIn = false;
    },

    [SET_AUTH](state, user) {
      state.isLoggedIn = true;
      state.user = user;
    },

    [LOGIN_FAILURE](state, error) {
      state.user = null;
      state.error = error;
    },

    [REMOVE_AUTH](state) {
      state.isLoggedIn = false;
      state.user = null;
    }
  },

  actions: {
    [REGISTER]({ commit }, credentials) {
      return new Promise((resolve, reject) => {
        AuthService.register(credentials)
          .then(user => {
            if (user) {
              commit(REGISTER_SUCCESS, user);
              resolve(user);
            }
          })
          .catch(error => {
            commit(REGISTER_FAILURE);
            reject(error);
          });
      });
    },

    [LOGIN]({ commit }, credentials) {
      return new Promise(resolve => {
        AuthService.login(credentials)
          .then(response => {
            if (response.status === 200) {
              commit(SET_AUTH, response.data);
              resolve(response.data);
            }
          })
          .catch(({ response }) => {
            let errMessage;
            if (response.status === 401) {
              errMessage = 'Username or password is incorrect';
            }
            commit(LOGIN_FAILURE, errMessage);
          });
      });
    },

    [LOGOUT]({ commit }) {
      return new Promise(resolve => {
        AuthService.logout().then(() => {
          commit(REMOVE_AUTH);
          resolve();
        });
      });
    }
  },

  getters: {
    username: state => state.user.username,
    avatar: state => state.user.avatar || '/img/default-avatar.png',
    isLoggedIn: state => state.isLoggedIn
  }
};
