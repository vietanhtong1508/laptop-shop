import CategoryService from '@/services/category.service';
import {
  GET_PARENT_CATEGORIES,
  GET_SUB_CATEGORIES,
  CREATE,
  UPDATE,
  DELETE
} from '../actions.type';
import {
  FETCH_START,
  FETCH_END,
  ADD_ITEM,
  SET_PARENT_CATEGORIES,
  SET_SUB_CATEGORIES,
  UPDATE_ITEM,
  GET_UPDATED_INDEX,
  DELETE_ITEM
} from '../mutations.type';

export const category = {
  namespaced: true,

  state: {
    parentCategories: [],
    subCategories: [],
    updatedIndex: -1,
    isLoadingSub: false,
    resultMessage: ''
  },

  mutations: {
    [FETCH_START](state) {
      state.isLoadingSub = true;
    },

    [SET_PARENT_CATEGORIES](state, categories) {
      state.parentCategories = categories;
      state.isLoading = false;
    },

    [SET_SUB_CATEGORIES](state, categories) {
      state.subCategories = categories;
      state.isLoadingSub = false;
    },

    [ADD_ITEM](state, { data, newCategory }) {
      if (newCategory.parentCategory) {
        state.subCategories.push(data);
      } else {
        state.parentCategories.push(data);
      }

      state.resultMessage = 'Added successfully';
    },

    [GET_UPDATED_INDEX](state, { id, isUpdateSub }) {
      const list = isUpdateSub ? state.subCategories : state.parentCategories;
      state.updatedIndex = list.findIndex(item => item.id === id);
    },

    [UPDATE_ITEM](state, { data, isUpdateSub }) {
      if (isUpdateSub) {
        Object.assign(state.subCategories[state.updatedIndex], data);
      } else {
        Object.assign(state.parentCategories[state.updatedIndex], data);
      }

      state.resultMessage = 'Updated successfully';
    },

    [DELETE_ITEM](state, { categoryId, isDeleteSubCat }) {
      let list = isDeleteSubCat ? state.subCategories : state.parentCategories;

      const deletedItem = list.find(item => item.id === categoryId);
      const deletedIndex = list.indexOf(deletedItem);
      list.splice(deletedIndex, 1);

      state.resultMessage = 'Deleted successfully';
    }
  },

  actions: {
    async [GET_PARENT_CATEGORIES]({ commit }) {
      commit(FETCH_START, null, { root: true });
      try {
        const { data } = await CategoryService.getParentCategories();
        commit(SET_PARENT_CATEGORIES, data);
        commit(FETCH_END, null, { root: true });
      } catch (error) {
        throw new Error(error);
      }
    },

    async [GET_SUB_CATEGORIES]({ commit }, parentId) {
      commit(FETCH_START);
      try {
        const { data } = await CategoryService.getSubCategories(parentId);
        commit(SET_SUB_CATEGORIES, data);
      } catch (error) {
        throw new Error(error);
      }
    },

    async [CREATE]({ commit }, newCategory) {
      try {
        const { data } = await CategoryService.create(newCategory);
        commit(ADD_ITEM, { data, newCategory });
      } catch (error) {
        throw new Error(error);
      }
    },

    async [UPDATE]({ commit }, { categoryToUpdate, isUpdateSub }) {
      try {
        const { id } = categoryToUpdate;
        commit(GET_UPDATED_INDEX, { id, isUpdateSub });
        const { data } = await CategoryService.update(id, {
          name: categoryToUpdate.name
        });
        commit(UPDATE_ITEM, { data, isUpdateSub });
      } catch (error) {
        throw new Error(error);
      }
    },

    async [DELETE]({ commit }, { categoryId, isDeleteSubCat }) {
      try {
        await CategoryService.delete(categoryId);
        commit(DELETE_ITEM, { categoryId, isDeleteSubCat });
      } catch (error) {
        throw new Error(error);
      }
    }
  }
};
