import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import { auth } from './modules/auth.module';
import { brand } from './modules/brand.module';
import { category } from './modules/category.module';

import { FETCH_START, FETCH_END } from './mutations.type';

Vue.use(Vuex);

const store = new Vuex.Store({
  plugins: [
    createPersistedState({
      storage: window.sessionStorage
    })
  ],

  state: {
    isLoading: false
  },

  modules: {
    auth,
    brand,
    category
  },

  mutations: {
    [FETCH_START](state) {
      state.isLoading = true;
    },

    [FETCH_END](state) {
      state.isLoading = false;
    }
  }
});

export default store;
