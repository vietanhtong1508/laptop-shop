export const REGISTER = 'register';
export const LOGIN = 'login';
export const LOGOUT = 'logout';
export const FETCH_ALL = 'fetchAll';
export const CREATE = 'create';
export const UPDATE = 'update';
export const DELETE = 'delete';

export const GET_PARENT_CATEGORIES = 'getParentCategories';
export const GET_SUB_CATEGORIES = 'getSubCategories';
