module.exports = {
  devServer: {
    port: 8070,
    proxy: {
      '/api': {
        target: 'http://localhost:8080'
      }
    }
  }
};
