package com.laptopshop.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.laptopshop.entity.Product;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import java.util.List;

public class BrandDto {

    private Integer id;

    @NotBlank(message = "Brand name is required")
    private String name;

    private String logo;

    private List<Product> productList;

    public BrandDto() {
    }

    public BrandDto(String name, String logo) {
        this.name = name;
        this.logo = logo;
    }

    public BrandDto(Integer id, String name, String logo) {
        this.id = id;
        this.name = name;
        this.logo = logo;
    }

    public Integer getId() {
        return id;
    }

    @ApiModelProperty(hidden = true)
    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @JsonIgnore
    public List<Product> getProductList() {
        return productList;
    }

    @ApiModelProperty(hidden = true)
    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

}
