package com.laptopshop.service;

import com.laptopshop.entity.Role;

import java.util.Set;

public interface RoleService {

    Set<Role> createRolesForNewUser(Set<String> requestRoles);

}
