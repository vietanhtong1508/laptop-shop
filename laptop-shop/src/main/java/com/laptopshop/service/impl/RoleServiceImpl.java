package com.laptopshop.service.impl;

import com.laptopshop.entity.Role;
import com.laptopshop.repository.RoleRepository;
import com.laptopshop.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.Set;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Set<Role> createRolesForNewUser(Set<String> requestRoles) {
        Set<Role> roles = new HashSet<>();

        if (CollectionUtils.isEmpty(requestRoles)) {
            Role userRole = roleRepository.findByName(Role.UserRole.CUSTOMER)
                    .orElseThrow(() -> new RuntimeException("Error: Role not found"));
            roles.add(userRole);
        } else {
            requestRoles.forEach(role -> {
                switch (role) {
                    case "OPERATOR":
                        Role operatorRole = roleRepository.findByName(Role.UserRole.OPERATOR)
                                .orElseThrow(() -> new RuntimeException("Error: Role not found"));
                        roles.add(operatorRole);
                        break;
                    case "ADMIN":
                        Role adminRole = roleRepository.findByName(Role.UserRole.ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role not found"));
                        roles.add(adminRole);
                        break;
                    default:
                        Role customerRole = roleRepository.findByName(Role.UserRole.CUSTOMER)
                                .orElseThrow(() -> new RuntimeException("Error: Role not found"));
                        roles.add(customerRole);
                }
            });
        }

        return roles;
    }

}
