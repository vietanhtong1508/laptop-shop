package com.laptopshop.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/all")
    public String allAccess() {
        return "Public content";
    }

    @GetMapping("/user")
    @PreAuthorize("hasAnyAuthority('CUSTOMER', 'OPERATOR', 'ADMIN')")
    public String userAccess() {
        return "User content";
    }

    @GetMapping("/operator")
    @PreAuthorize("hasAnyAuthority('OPERATOR', 'ADMIN')")
    public String operatorAccess() {
        return "Operator board";
    }

    @GetMapping("/admin")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String adminAccess() {
        return "Admin board";
    }

}
