package com.laptopshop.payload.request;

import java.util.Set;

public class SignUpRequest {

    private String email;

    private String username;

    private String password;

    private Set<String> roles;

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Set<String> getRoles() {
        return roles;
    }

}
