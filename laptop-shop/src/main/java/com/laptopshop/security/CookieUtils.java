package com.laptopshop.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpCookie;
import org.springframework.http.ResponseCookie;
import org.springframework.stereotype.Component;

@Component
public class CookieUtils {

    @Value("${laptopshop.app.cookieExpiration}")
    private int cookieExpirationInSeconds;

    public HttpCookie createCookie(String jwt) {
        return ResponseCookie.from("accessToken", jwt)
                .path("/")
                .maxAge(cookieExpirationInSeconds)
                .httpOnly(true)
                .build();
    }

}
